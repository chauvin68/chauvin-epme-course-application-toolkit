﻿namespace Ecat.Models
{
    public class UserToken
    {
        public int PersonId { get; set; }
		public int CourseId { get; set; }
        public string MpCourseRole { get; set; }
    }
}
