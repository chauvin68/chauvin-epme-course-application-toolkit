﻿namespace Ecat.Models
{
    public class CogResult
    {
        public int Id { get; set; }
        public string MpCogOutcome { get; set; }
        public float MpCogScore { get; set; }
    }
}
