﻿import 'breeze'
import ICommon from 'core/service/common'
import * as appVar from 'appVars'

export default class EcEmFactory {
    static serviceId = 'data.emfactory';

    static $inject = ['$injector', ICommon.serviceId];

    constructor(private $injector: angular.auto.IInjectorService,
        private common: ICommon) {
    }


    getRepo(reporName: string): any {
        const repo = this.$injector.get(`data.${reporName}`);
        return repo;
    }

    getNewManager(apiResourceName: appVar.EcMapApiResource, clientExtensions?: Array<ecat.entity.IEntityExtension>): breeze.EntityManager {
        breeze.NamingConvention.camelCase.setAsDefault();
        new breeze.ValidationOptions({ validateOnAttach: false }).setAsDefault();

        const serviceName = this.common.appEndpoint + apiResourceName;
        const metaDataStore = this.createMetadataStore(clientExtensions);
        //this.registerResourceTypes(metaDataStore, apiResourceName);
        return new breeze.EntityManager({
            serviceName: serviceName,
            metadataStore: metaDataStore
        });
    }

    //#region Internal Api
    private createMetadataStore(clientExtensions: Array<ecat.entity.IEntityExtension>): breeze.MetadataStore {
        const metadataStore = new breeze.MetadataStore();
        if (clientExtensions && clientExtensions.length > 0) {
            clientExtensions.forEach((ext) => {
                metadataStore.registerEntityTypeCtor(ext.entityName, ext.ctorFunc, ext.initFunc);
            });
        }
        return metadataStore;
    }

    registerResourceTypes(metadataStore: breeze.MetadataStore, apiResourceName: appVar.EcMapApiResource) {
        const apiResources = this.common.resourceNames[apiResourceName as string];

        for (let resourceEntity in apiResources) {
            if (apiResources.hasOwnProperty(resourceEntity)) {
                const selectedResource = apiResources[resourceEntity];
                if (typeof selectedResource !== 'string') {
                    const resource = selectedResource as ecat.IApiResource;
                    metadataStore.setEntityTypeForResourceName(resource.resourceName, resource.entityType);
                }
            }
        }
    }

//#endregion
}

