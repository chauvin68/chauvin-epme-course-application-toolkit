module Ecat.Models {
	export const enum EpmeSchool {
		Bcee = 0,
		Afsncoa = 1,
		Clc = 2,
		Ncoa = 3,
		Keesler = 4,
		Sheppard = 5,
		Tyndall = 6,
		Epmeic = 7
	}
	export const enum EcRoles {
		Unknown = 0,
		SysAdmin = 1,
		Designer = 2,
		CrseAdmin = 3,
		Facilitator = 4,
		Student = 5,
		External = 6
	}
}

